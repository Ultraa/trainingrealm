//
//  SecondViewController.swift
//  TrainingRealm
//
//  Created by User Userovich on 24.04.2018.
//  Copyright © 2018 User Userovich. All rights reserved.
//

import UIKit
import RealmSwift

class SecondViewController: UIViewController {

    @IBOutlet weak var upperTF: UITextField!
    
    var currentItem: Item?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let item = currentItem {
            upperTF.text = item.firstName
        }

        // Do any additional setup after loading the view.
    }

    @IBAction func saveButtonClicked(_ sender: UIButton) {
        let item = Item()
        
        item.firstName = upperTF.text!
        if (currentItem != nil) {
            item.itemID = (currentItem?.itemID)!
        }
        
//        DBManager.sharedInstance.addData(object: item)
        RealmStore.shared.realm.add(item, update: true)
//        RealmStore.shared.realm.write {
//            item = item
//        }
        
        self.dismiss(animated: true) { }
    }
    
    @IBAction func deleteButtonClicked(_ sender: UIButton) {
        if let item = currentItem {
//            DBManager.sharedInstance.deleteFromDb(object: item)
            RealmStore.shared.realm.delete(item)
            
            self.dismiss(animated: true) { }
        }
    }

}
