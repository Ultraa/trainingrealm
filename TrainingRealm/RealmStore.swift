//
//  RealmStore.swift
//  TrainingRealm
//
//  Created by User Userovich on 24.04.2018.
//  Copyright © 2018 User Userovich. All rights reserved.
//

import UIKit
import RealmSwift

public class RealmStore {
    
    public static var shared: RealmStore {
        return RealmStore()
    }
    
    let realm: Realm!
    
    init() {
        realm = try! Realm()
    }
    
    init(inMemoryIdentifier: String) {
        realm = try! Realm(configuration: Realm.Configuration(inMemoryIdentifier: inMemoryIdentifier))
    }
    
    init(with realmConfig: Realm.Configuration) {
        realm = try! Realm(configuration: realmConfig)
    }
    
    func safeWriteToRealm(block: () -> Void) {
        try! realm.write {
            block()
        }
    }
    
    func insert(_ item: Item) {
        try! realm.write {
            realm.create(Item.self, value: item, update: true)
        }
    }
    
    func getItemData(with compoundKey: String) -> Item? {
        let objects = Array(realm.objects(Item.self).filter({ $0.compoundKey == compoundKey }))
        return objects.isEmpty ? nil : objects[0]
    }
    
    public func cleanAllData() {
        try! realm.write {
            realm.delete(realm.objects(Item.self))
            // realm.deleteAll()
        }
    }
}
