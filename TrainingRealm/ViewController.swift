//
//  ViewController.swift
//  TrainingRealm
//
//  Created by User Userovich on 24.04.2018.
//  Copyright © 2018 User Userovich. All rights reserved.
//

import UIKit
import RealmSwift

class ViewController: UIViewController {

    @IBOutlet weak var myTableView: UITableView!
//    var objects: Results<Item>!
    var realm = try! Realm()
    lazy var contacts: Results<Item> = { self.realm.objects(Item.self) }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myTableView.dataSource = self
        myTableView.delegate = self
        
        // Печать пути реалм даты
        print(Realm.Configuration.defaultConfiguration.fileURL!)
        
        populateDefaultContacts()
//        objects = realm.objects(Item.self).sorted(byKeyPath: "textString")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        myTableView.reloadData()
    }

    @IBAction func addButtonClicked(_ sender: UIBarButtonItem) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "secondView") as! SecondViewController
        
        vc.currentItem = nil
        
        self.present(vc, animated: false, completion: nil)
    }
    
    
    func populateDefaultContacts() {
        if contacts.isEmpty {
            try! realm.write() {
                let defaultContacts = ["AAA"]
                
                for contact in defaultContacts {
                    let newContact = Item()
                    newContact.firstName = contact
                    self.realm.add(newContact)
                }
            }
            
            contacts = realm.objects(Item.self)
        }
    }
    
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
        //        return DBManager.sharedInstance.getDataFromDB().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = myTableView.dequeueReusableCell(withIdentifier: "CellIdentifier", for: indexPath) as UITableViewCell
        
        let index = indexPath.row
        
        let contact = contacts[index]
        cell.textLabel?.text = contact.firstName
        
//        let item = DBManager.sharedInstance.getDataFromDB()[index] as Item
//        cell.textLabel?.text = item.textString
        
//        cell.textLabel?.text = objects[index].firstName
        
        return cell
    }

    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "secondView") as! SecondViewController
        let index = indexPath.row
        vc.currentItem = contacts[index]
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath.row > -1) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "secondView") as! SecondViewController
            
            let index = indexPath.row
            
//            let item = DBManager.sharedInstance.getDataFromDB()[index] as Item
            let item = contacts[index]
            
//            let item = RealmStore.shared.getItemData(with: "\(index)")
            
            vc.currentItem = item
            
            self.present(vc, animated: false, completion: nil)
        }
    }
}
