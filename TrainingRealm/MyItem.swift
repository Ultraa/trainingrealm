//
//  MyItem.swift
//  TrainingRealm
//
//  Created by User Userovich on 24.04.2018.
//  Copyright © 2018 User Userovich. All rights reserved.
//

import UIKit
import RealmSwift

class Item: Object {
    
    @objc dynamic var itemID: Int = 0 {
        didSet {
            compoundKey = compoundKeyValue()
        }
    }
    
    @objc public dynamic var compoundKey: String = ""
    
    @objc private func compoundKeyValue() -> String {
        return "\(itemID)"
    }

    
    @objc dynamic var firstName = ""
    @objc dynamic var lastName = ""
    @objc dynamic var phoneNumber = ""
    
    required convenience init(itemID: Int, firstName: String, lastName: String, phoneNumber: String) {
        self.init()
        self.itemID = itemID
        self.firstName = firstName
        self.lastName = lastName
        self.phoneNumber = phoneNumber
    }
    
    override static func primaryKey() -> String? {
        return "compoundKey"
    }
}
